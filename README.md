[![coverage report](https://framagit.org/ludolas/k-project/badges/develop/coverage.svg)](https://framagit.org/ludolas/k-project/-/commits/develop) (tag is for back)

# K Project

## Introduction ##
The project has ben created for an evaluation assignment, in a given time.  
I have played the game **_the hard way_**, with the fewest libraries, even not express as it was not mentionned so i had to 
create a whole backend framework base.     
The main philosophy is to be able to have a working project with quality basis : 
* CI
* unit testing,
* ease to install / test / deploy
* ...
  
so that new developers can join project and follow best practivies the more easily possible.  
Unfortunately, choices had to be made and the coverage is under the wanted ratio, but the main features are covered and the 
most type of components so that covering the rest of the code should be a repetitve task with no main difficulty (let's hope !)

## Default user journey ##
Let's say that this application is a geocatching one.

Anyone can create a user (and a admin!)  
An admin can create an order like 'find a girafe'  
the order becomes available to other users who can add comments to explain where is the giraffe  
Admin can answer, delete comments and delete users.       
  
**Demo video [here](./doc/demo.mov)**

<!-- blank line -->
<figure class="video_container">
  <video controls="true" allowfullscreen="true">
    <source src="doc/demo.webm" type="video/webm">
  </video>
</figure>
<!-- blank line -->

## More info ##
Don't hesitate to take a look at [contributing](./CONTRIBUTING.md) to get more infos
