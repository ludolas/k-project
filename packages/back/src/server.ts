import {ControllerInterface} from './controllers/ControllerInterface';
import {IncomingMessage, ServerResponse} from 'http';
import http from 'http';
import {URL} from 'url';
import {HttpError} from './errors/http.error';

export class RequestParam {
    pathParams: {[key: string]: string} = {};
    queryParams: {[key: string]: string|string[]} = {};
    postParams: {[key: string]: string} = {};
    headersParams: {[key: string]: string} = {};
}

type ControllerList = { [ key: string ]: {instance: ControllerInterface, routes: { [ key: string ]: any }}};

export class Server {
    private controllers: ControllerList = {};
    private routes: { [ key: string ]: any } = {};
    private basePath: string = '/api/v1/';
    private port: number = 3000;
    private hostname: string = 'localhost';
    private instance!: http.Server;

    /**
     * Call this method to create na http server, instantiate routes and listen to incoming request
     *
     * @param controllers : the list of controller entry points with their knwo routes
     * @param hostname
     * @param port
     * @param basePath
     */
    public init(controllers: ControllerList ,hostname: string = '127.0.0.1', port: number = 3001, basePath: string = '/api/v1/') {
        this.controllers = controllers;

        this.hostname = hostname;
        this.port = port;
        this.basePath = basePath;

        return this.instance = http.createServer((req: IncomingMessage, res: ServerResponse) => {
            const chunks: any[] | Uint8Array[] = [];

            this.setCORSHeaders(res);
            if ( req.method === 'OPTIONS' ) {
                res.writeHead(200);
                res.end();
                return;
            }

            req.on('data', chunk => chunks.push(chunk));
            req.on('end',  () => this.handleRequest(chunks, req, res)
                .then(
                    result => res.end(JSON.stringify(result)),
                    e => {
                        if (e instanceof HttpError) {
                            res.statusCode = e.httpCode;
                            res.end(e.message);
                        } else {
                            console.log('unknown error thrown', e);
                            res.statusCode = 500;
                            res.end(e.message);
                        }
                    }));

        });
    }

    /**
     * Tries to find the matching route adn returns the result
     *
     * @param data : the request body sent by the client
     * @param req
     * @param res
     */
    public async handleRequest(data: any[] | Uint8Array[], req: IncomingMessage, res: ServerResponse) {
        const postData = data.length > 0 ? JSON.parse(Buffer.concat(data).toString()) : {};
        return this.findRoute(req, res, postData);
    }

    /**
     * To be called on service shutdown
     */
    public close(){
        this.instance.close();
    }

    /**
     * sets most open CORS headers
     * TODO not for production, only really known clients should be allowed
     * @param res
     * @private
     */
    private setCORSHeaders(res: ServerResponse){
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Request-Method', '*');
        res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, DELETE');
        res.setHeader('Access-Control-Allow-Headers', '*');
    }
    /**
     * parses the given route and tries to find the matching controller / method then calls it
     *
     * @param req
     * @param res
     * @param postData
     * @private
     */
    private async findRoute(req: IncomingMessage, res: ServerResponse, postData: any) {
        if (!req.url) {
            throw new HttpError(404);
        }
        if (!req.method) {
            throw new HttpError(400, 'no method specified');
        }

        const reqUrl = new URL(req.url, `http://${this.hostname}:${this.port}${this.basePath}`);

        const parsedParam = this.parseParams(reqUrl, req, postData);

        let currentPath = this.routes;
        let currentCtrl;

        // runs through each declared controllers
        currentCtrl = Object.values(this.controllers).find(ctrl => {
            currentPath = ctrl.routes;
            // parse wanted route
            reqUrl.pathname.replace(this.basePath, '').split('/')
                .find(
                    (part: string) => {
                        // find matching declared route
                            if (currentPath.hasOwnProperty(part)) {
                                // global route param
                                currentPath = currentPath[part];
                            } else {
                                // try to find a route with dynamic param
                                const dynParamKey = Object.keys(currentPath).find(param => param.indexOf(':') === 0);

                                if (dynParamKey) {
                                    parsedParam.pathParams[dynParamKey] = part;
                                    currentPath = currentPath[dynParamKey];
                                } else {
                                    currentPath = {};
                                }
                            }
                            if (req.method && currentPath.hasOwnProperty(req.method)) {
                            }
                    });
            if (Object.values(currentPath).length > 0) {
                return true;
            }
        });

        // all controllers have been run through and no matching mtehod have been found
        if (!Object.values(currentPath).length || !currentCtrl) {
            throw new HttpError(404);
        }

        // might throw
        Server.setDefaultResponse(res);
        // route known, calling matching method
        return await currentPath[req.method].apply(currentCtrl.instance, [parsedParam, res]);
    }

    /**
     *
     * with params parsed as :
     *  - pathParams : parameters found in the main route, i.e 12 as :userId in /users/12 if route has been defined as /users/:userId,
     *        Please note that these params will be filled when parsing client desired route
     *  - queryParams : parameters in route after '?' , ie route?search=needle
     *  - postParams : parameters in body, note that server should accept only JSON for that v0
     *  - headerParams : parameters found as header, only a custom 'Authentication: UserID xxxxxx' is allowed for now
     *
     * @param reqUrl
     * @param req
     * @param postData
     * @private
     */
    private parseParams(reqUrl: URL, req: IncomingMessage, postData: any): RequestParam {
        const parsedParam: RequestParam = {pathParams: {}, queryParams: {}, postParams: {}, headersParams: {}};

        parsedParam.queryParams = this.parseQueryParams(reqUrl);
        parsedParam.postParams = postData;
        parsedParam.headersParams = { userId: (req.headers.authorization || '').replace('UserId ', '')}

        return parsedParam;
    }

    /**
     * Optimistically prepare default response with OK status code and JSON content-type
     *
     * @param res mutates
     * @private
     */
    private static setDefaultResponse(res: ServerResponse) {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
    }

    /**
     * creates a key value object containing parsed query params from URL.
     * If some param has multiple values, the property will be an array of theses values
     * i.e. ?param1=val1&param2=val2a&param2=val2b => {param1: 'val1', param2: ['val2a', 'val2b' ]}
     *
     * @param reqUrl
     * @private
     */
    private parseQueryParams(reqUrl: URL): {[key: string]: string|string[]} {
        const result: {[key: string]: string|string[]} = {};

        reqUrl.searchParams.forEach((val: string, key: string) => {
                const values = reqUrl.searchParams.getAll(key);
                result[key] = values.length === 1 ? values[0]: values;
            }
        );

        return result;
    }
}
