import mongoose, {Document, Schema } from 'mongoose';
import { UserInterface } from './user.model';
import {OrderInterface} from './order.model';
import {ChannelInterface} from './channel.model';

const OrderCommentSchema: Schema = new Schema({
    channel: { type: Schema.Types.ObjectId, required: true },
    from: { type: Schema.Types.ObjectId, required: true },
    content: { type: String, required: true },
}, {timestamps: true});

export interface OrderCommentInterface extends Document {
    channel: ChannelInterface['_id'];
    from: UserInterface['_id'];
    content: string;
}

export const OrderComment = mongoose.model<OrderCommentInterface>('OrderComment', OrderCommentSchema);
