import {AppModules} from '../index';

export class Model {
    protected mongoose
    constructor() {
        this.mongoose = AppModules.mongoose;
    }
}
