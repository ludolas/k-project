import mongoose, {Document, Schema } from 'mongoose';
import { UserInterface } from './user.model';

const OrderSchema: Schema = new Schema({
    title: { type: String, required: true }
});

export interface OrderInterface extends Document {
    title: string;
}

export const Order = mongoose.model<OrderInterface>('Order', OrderSchema);
