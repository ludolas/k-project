import mongoose, {Document, Schema } from 'mongoose';
import {OrderComment} from './orderComment.model';

const UserSchema: Schema = new Schema({
    name: { type: String, required: true, unique: true },
    // shortcut, user could have multiple roles, here we just store whether its a superadmin or not
    isAdmin: { type: Boolean, default: false},
});

// simulate some cascade, delete user comments when user is deleted
UserSchema.pre('findOneAndDelete',  {query: true}, function() {
    const idToDelete = (this as any)._conditions._id;
    OrderComment.deleteMany({from: idToDelete}).exec();
});

export interface UserInterface extends Document {
    name: string;
}

export const User = mongoose.model<UserInterface>('User', UserSchema);
