import mongoose, {Document, Schema } from 'mongoose';
import { UserInterface } from './user.model';
import {OrderInterface} from './order.model';
import {OrderComment} from './orderComment.model';

const ChannelSchema: Schema = new Schema({
    name: { type: String, unique: true},
    geoReferenceId: { type: Number, required: false},
    user: { type: Schema.Types.ObjectId, required: true },
    order: { type: Schema.Types.ObjectId, required: true },
});

// simulate some cascade : comments are deleted when deleting channel
ChannelSchema.pre('findOneAndDelete',  {query: true}, function() {
    const idToDelete = (this as any)._conditions._id;

    OrderComment.deleteMany({channel: idToDelete}).exec();
});

export interface ChannelInterface extends Document {
    name: string,
    geoReferenceId: number;
    user: UserInterface['_id'];
    order: OrderInterface['_id'];
}

export const Channel = mongoose.model<ChannelInterface>('Channel', ChannelSchema);
