export class HttpError extends Error {
    httpCode: number;

    constructor(httpCode: number, message?: string) {
        super(message);
        this.httpCode = httpCode;

        // to be able to detect instanceof
        Object.setPrototypeOf(this, HttpError.prototype);
    };
}
