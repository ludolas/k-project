export class ParameterError extends Error {

    constructor(message?: string) {
        super(message);
        Object.setPrototypeOf(this, ParameterError.prototype);
    };
}
