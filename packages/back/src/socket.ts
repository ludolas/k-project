import { Server} from 'http';
import { Server as SocketServer} from 'socket.io';
import {AppModules} from './index';
import {Logger} from 'winston';
const env = process.env;

export class Socket {
    private instance!: SocketServer;
    private logger: Logger;

    constructor() {
        this.logger = AppModules.logger;
    }

    // FIXME stateful ! should not be local but stored on a shared redis service by instance
    private subscriptionMap: {[ channelName: string ]: any[]} = {};

    /**
     * creates a socker server and stores it
     * @param httpServer
     */
    public init(httpServer: Server) {
        const hostname = env.HOST || '127.0.0.1';
        const port = env.PORT && parseInt(env.PORT, 10) || 3001;

        this.instance = new SocketServer(httpServer, {
            transports: [ 'websocket', 'polling'],
            allowEIO3: true,
            cors: {
                origin: "http://localhost:8080", // FIXME should be dynamic
                methods: ["GET", "POST"],
                credentials: true
            }
        });
        this.initListeners();
    }

    /**
     * simple shortcut to create map entry or retrieve the one already existing
     * @param subscriptionMap
     * @param channel
     * @private
     */
    private getOrInitIndex(subscriptionMap: {[ channelName: string ]: any[]}, channel: string) {
        return subscriptionMap[channel] || [];
    }

    /**
     * Plugs socket events
     * @private
     */
    private initListeners(): void {
        this.instance.on('connection', client => {
            this.logger.info('[SOCKET] Connected client');

            client.on('subscribeChannel', async (channelId, callback) => {
                this.logger.info('[SOCKET] client subscribe channe', channelId);
                this.getOrInitIndex(this.subscriptionMap, channelId).push(client);
            });
            client.on('unsubscribeChannel', async (data, callback) => {
                this.logger.info('[SOCKET] client unsubscribe channe', data);
                // TODO remove connection from subscriptionmap
            });
        });
    }

    /**
     * Emits the event to all connected clients (FIXME!)
     * @param eventName the event that should trigger some action client side for clients listening to that event
     * @param data
     */
    public emit(eventName: string, data: any){
        // TODO shortcut for lack of time, emitting on each connected client !!
        // we should simple emit to clients that have subscribed to concerned channel
        // as stored in the subscriptionMap
        this.logger.info(`[SOCKET] emitting ${eventName}`);
        this.instance.emit(eventName, data);
    }
}
