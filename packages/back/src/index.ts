import { Server } from './server';
import winston, { format, transports, Container } from 'winston';
import mongoose, {mongo} from 'mongoose';
import {ErrorService} from './services/error.service';
import {UserController} from './controllers/user.controller';
import {AuthController} from './controllers/auth.controller';
import {OrderController} from './controllers/order.controller';
import {OrderCommentController} from './controllers/orderComment.controller';
import {ChannelController} from './controllers/channel.controller';
import {Socket} from './socket';

const env = process.env;
const hostname = env.HOST || '127.0.0.1';
const port = env.PORT && parseInt(env.PORT, 10) || 3001;
const basePath = env.BASE_PATH || '/api/v1/';
const mongoHost = env.MONGO_HOST || '127.0.0.1';
const appRoot = __dirname;

export const logger =
    winston.loggers.add('KP_LOGGER', {
    level: 'info', // TODO store as env
    exitOnError: false,
    format: format.json(),
    transports: [
        new transports.File({ filename: `${appRoot}/logs/kp.log` }), // TODO store as env
    ],
});


if (process.env.NODE_ENV !== 'production') {
    logger.add(new winston.transports.Console({
        format: winston.format.simple(),
    }));
}

export const server = new Server();

if (process.env.NODE_ENV !== 'test') {
    mongoose.connect(`mongodb://${mongoHost}:27017/k-proj`, {useNewUrlParser: true, useUnifiedTopology: true},
        () => logger.debug("MongoDB connected"));
    // avoid node warning at startup, @see https://stackoverflow.com/a/51962721
    mongoose.set('useNewUrlParser', true);
    mongoose.set('useFindAndModify', false);
    mongoose.set('useCreateIndex', true);
}

export const AppModules: Record<string, any> = { httpServer: server, mongoose, logger};

AppModules.errorService = new ErrorService();

AppModules.controllers = {
    user: new UserController(),
    auth: new AuthController(),
    order: new OrderController(),
    orderComment: new OrderCommentController(),
    channel: new ChannelController(),
}

// TODO to be reviewed, not maintainable, find a better way to instantiate and declare controllers and routes, ideally with decorators
const controllersList = {
    userController: {
        instance: AppModules.controllers.user,
            routes: AppModules.controllers.user.getRoutes(),
    },
    authController: {
        instance: AppModules.controllers.auth,
            routes: AppModules.controllers.auth.getRoutes(),
    },
    orderController: {
        instance: AppModules.controllers.order,
            routes: AppModules.controllers.order.getRoutes(),
    },
    channelController: {
        instance: AppModules.controllers.channel,
            routes: AppModules.controllers.channel.getRoutes(),
    }
}

export const serv = server.init(controllersList, hostname, port, basePath);

export const socket = new Socket();
socket.init(serv);

if (process.env.NODE_ENV !== 'test') {
    serv.listen(port, hostname, () => {
        if (process.env.NODE_ENV !== 'test') {
            logger.info(`Server running at http://${hostname}:${port}/`);
        }
    });
}
