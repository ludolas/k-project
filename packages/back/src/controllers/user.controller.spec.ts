// import { UserController } from './user.controller';
import { AppModules } from '../index';
const mockingoose = require('mockingoose');
const httpMocks = require('node-mocks-http');
import {User} from '../models/user.model';
import {RequestParam, Server} from '../server';
import {ControllerInterface} from './ControllerInterface';
import * as http from 'http';
import {IncomingMessage} from 'http';
import {HttpError} from '../errors/http.error';
var MockReq = require('mock-req');
var MockRes = require('mock-res');


describe('Test user.controller.ts', () => {
    let controller: ControllerInterface;
    let server: Server;

    beforeAll(() => {
        controller = AppModules.controllers.user;
        server = new Server();
        server.init({
            userController: {
                instance: controller,
                routes: controller.getRoutes(),
            }});
        //AppModules.controllers = [new UserController()];
       // const sampleRequestSpy = jest.spyOn(UserController as any, 'sampleRequest');
        const _doc = {
            _id: '507f191e810c19729de860ea',
            name: 'name'
        }

        mockingoose(User).toReturn(_doc, 'save');
    });

    afterAll(done => {
        AppModules.httpServer.close();
        AppModules.mongoose.connection.close();
        done();
    });

    test('should greet', async (done) => {
        let res = new MockRes({});
        var req = new MockReq({
            method: 'GET',
            url: 'http://127.0.0.1:3001/api/v1/users/sample?name=greetMePlease',
        });
        const result = await server.handleRequest([], req, res);

        done();
    });

    test('should throw 404 for unknwon user', async (done) => {
        let res = new MockRes({});
        var req = new MockReq({
            method: 'GET',
            url: 'http://127.0.0.1:3001/api/v1/users/fafafa',
        });

        mockingoose(User).toReturn(new Error('not found'), 'findOne');

        expect(await server.handleRequest([], req, res)).toBeFalsy();
        done();
    });

    test('should return known user found by id', async (done) => {
        const userId = "6086843aea57568f09eec75b";
        const fakeDoc = {"_id": userId, name: 'fakeMongooseUser'};

        mockingoose(User).toReturn(fakeDoc, 'findOne');
        let res = new MockRes({});
        var req = new MockReq({
            method: 'GET',
            url: 'http://127.0.0.1:3001/api/v1/users/fafafa',
        });

        server.handleRequest([], req, res).then(doc => {
            expect(JSON.parse(JSON.stringify(doc))).toMatchObject(fakeDoc);
        });
        done();
    });
});
