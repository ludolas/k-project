import {ControllerInterface} from './ControllerInterface';
import {RequestParam} from '../server';
import {ServerResponse} from 'http';
import {HttpError} from '../errors/http.error';
import {AbstractController} from './AbstractController';
import {AuthService} from '../services/auth.service';
import {UserInterface} from '../models/user.model';

export class AuthController extends AbstractController implements ControllerInterface {
    authService: AuthService;

    constructor() {
        super();
        this.authService = new AuthService();
    }
    /**
     * @see ControllerInterface.getRoutes
     */
    getRoutes() {
        return {
            'auth': {
                'login': {
                    POST: this.login
                },
                'register': {
                    POST: this.createUserAndLogin
                },
                'logout': {
                    GET: this.logout
                }
            }
        }
    }

    private async login(requestParams: RequestParam, res: ServerResponse): Promise<UserInterface> {
        const name = this.checkParamUnicity(requestParams.postParams['name']);

        const user = await this.authService.login(name);

        if(!user) {
            throw new HttpError(404);
        }

        return user;
    }

    private logout(requestParams: RequestParam, res: ServerResponse): boolean {
        return true;
    }

    private async createUserAndLogin(requestParams: RequestParam, res: ServerResponse): Promise<UserInterface> {
        const name = this.checkParamUnicity(requestParams.postParams['name']);
        const admin = !!requestParams.postParams['isAdmin'];

        const newUser = await this.authService.register(name, admin);

        if(!newUser) {
            throw new HttpError(404);
        }

        return newUser;
    }
}
