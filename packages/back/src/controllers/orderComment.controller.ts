import {ControllerInterface} from './ControllerInterface';
import {RequestParam} from '../server';
import {ServerResponse} from 'http';
import {AbstractController} from './AbstractController';
import {OrderService} from '../services/order.service';
import {OrderInterface} from '../models/order.model';
import {OrderCommentService} from '../services/orderComment.service';
import {OrderCommentInterface} from '../models/orderComment.model';

export class OrderCommentController extends AbstractController implements ControllerInterface {

    constructor() {
        super();

    }
    /**
     * @see ControllerInterface.getRoutes
     */
    getRoutes() {
        return {
            'comments': {
                GET: this.getComments,
                POST: this.createOrderComment,
            }
        }
    }

    private createOrderComment(requestParams: RequestParam, res: ServerResponse): Promise<OrderCommentInterface> {
        const orderCommentService = new OrderCommentService();
        const content = requestParams.postParams['content'];
        // FIXME should be checked with userId from bearer
        const userId = requestParams.headersParams['userId'];

        const channelId = this.checkParamUnicity(requestParams.pathParams[':channelId']);

        // Multi puprose method as there are multi route entries, should be split in different functions
        if(channelId) {
            // '/channels/{channelId}/comments
            return orderCommentService.createInChannel(userId, channelId, content);
        } else {
            // '/orders/{orderId}/comments?geoReferenceId=
            const orderId = requestParams.pathParams[':orderId'];
            const geoReferenceId = requestParams.postParams['geoReferenceId'] && Number.parseInt(requestParams.postParams['geoReferenceId'], 10) || undefined;
            const content = requestParams.postParams['content'];
            // FIXME

            // TODO check mandatory inputs

            // optimistic approach : we choose to try to insert and then catch exception instead of trying to check manually if entity exists
            return orderCommentService.createOrderComment(userId, orderId, content, geoReferenceId)
                .then(order => order)
                .catch(err => {
                    console.log('error when creating order', err);
                    this.errorService.mongooseToAPIerror(err);
                    return err;
                });
        }

    }

    private getComments(requestParams: RequestParam, res: ServerResponse): Promise<OrderCommentInterface[]> {
        console.log('Comments');
        const orderCommentService = new OrderCommentService();
        // FIXME should be checked with userId from bearer
        const channelId = this.checkParamUnicity(requestParams.pathParams[':channelId']);
        if (channelId) {
            // '/channels/{channelId}/comments
            return orderCommentService.listForChannel(channelId);
        } else {
            // '/channels/{channelId}/comments?geoReferenceId=
            const orderId = requestParams.pathParams[':orderId'];
            const geoReferenceId = Number.parseInt(this.checkParamUnicity(requestParams.queryParams['geoReferenceId']), 10);
            const userId = requestParams.headersParams['userId'];
            // FIXME

            // TODO check mandatory inputs

            // optimistic approach : we choose to try to insert and then catch exception instead of trying to check manually if entity exists
            return orderCommentService.listForUser(userId, orderId, geoReferenceId)
                .then(orderComments => orderComments)
                .catch(err => {
                    console.log('error when creating order', err);
                    this.errorService.mongooseToAPIerror(err);
                    return [];
                });
        }
    }
}
