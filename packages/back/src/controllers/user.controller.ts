import {ControllerInterface} from './ControllerInterface';
import {UserService} from '../services/user.service';
import {URL} from 'url';
import {RequestParam} from '../server';
import {ServerResponse} from 'http';
import {ParameterError} from '../errors/parameter.error';
import {HttpError} from '../errors/http.error';
import {AbstractController} from './AbstractController';
import {OrderController} from './order.controller';
import {UserInterface} from '../models/user.model';

export class UserController extends AbstractController implements ControllerInterface {
    private orderController: OrderController;
    private userService: UserService;

    constructor() {
        super();
        this.orderController = new OrderController();
        this.userService = new UserService();
    }

    /**
     * @see ControllerInterface.getRoutes
     */
    getRoutes() {
        return {
            'users': {
                GET: this.findUsers,
                POST: this.createUser,
                'sample': {
                    GET: this.sampleRequest
                },
                ':userId': {
                    GET: this.getUserDetails,
                    DELETE: this.deleteUser,
                    PUT: this.updateUser,
                    'details': {
                        ':detailId': {
                            GET: this.getUserDetails
                        }
                    },
                }
            }
        }
    }

    /**
     * Simple sample GET request tot test the API that should return an object with {"text": "hello ${name}"}
     *
     * GET /users/sample?name=NameToGreet
     * queryParams :
     *  - name ((not so) mandatory: string
     *
     * @param requestParams
     * @param res
     * @private
     */
    private sampleRequest(requestParams: RequestParam, res: ServerResponse): {text: string} {
        const name = this.checkParamUnicity(requestParams.queryParams['name']);

        return this.userService.sayMyName(name);
    }

    /**
     * Creates a user
     *
     * POST /users/
     * postParams :
     *  - name(mandatory): string
     *
     * @param requestParams
     * @param res
     * @private
     */
    private createUser(requestParams: RequestParam, res: ServerResponse): Promise<Boolean> {
        // FIXME should retrieve and use a single instance
        const name = requestParams.postParams['name'];

        // optimistic approach : we choose to try to insert and then catch exception instead of trying to check manually if entity exists
        return this.userService.createUser(name).then(user => {
            res.statusCode = 201;
            return true;
        }).catch(err => {
            this.errorService.mongooseToAPIerror(err);
            return false;
        });
    }

    /**
     * Retrieves ALL known infos for a given userId
     *
     * GET /users/:userId/details
     * pathParams:
     *  - userId : string : the user id attributed by database when created
     *
     * @param requestParams
     * @param res
     * @private
     */
    private getUserDetails(requestParams: RequestParam, res: ServerResponse): Promise<UserInterface> {
        const userId = requestParams.pathParams[':userId'];

        return this.userService.getUserDetails(userId).then(
            resp => resp,
            err => {
                this.catchHttpError(err, res);
                return false;
            }
        );
    }

    /**
     * Returns ALL users matching queryParams criteria
     * TODO pagination
     *
     * GET /users?name=details
     * queryParams:
     *  - name : string : the name of the user to find
     *
     * @param requestParams
     * @param res
     * @private
     */
    private findUsers(requestParams: RequestParam, res: ServerResponse): Promise<UserInterface[]> {
        const name = this.checkParamUnicity(requestParams.queryParams['name']);

        return this.userService.findUsersByName(name).then(
            resp => resp,
            err => {
                this.catchHttpError(err, res);
                return [];
            }
        );
    }

    private deleteUser(requestParams: RequestParam, res: ServerResponse): Promise<boolean> {
        const userId = requestParams.pathParams[':userId'];

        return this.userService.delete(userId).then(
            resp => resp,
            err => {
                this.catchHttpError(err, res);
                return false;
            }
        );
    }

    private updateUser(requestParams: RequestParam, res: ServerResponse): Promise<UserInterface> {
        const userId = requestParams.pathParams[':userId'];
        const userFromReq = {
            name: this.checkParamUnicity(requestParams.postParams['name']),
            isAdmin: !!this.checkParamUnicity(requestParams.postParams['isAdmin']),
        };

        return this.userService.update(userId, userFromReq.name, userFromReq.isAdmin).then(
            resp => resp,
            err => {
                this.catchHttpError(err, res);
                return err;
            }
        );
    }
}
