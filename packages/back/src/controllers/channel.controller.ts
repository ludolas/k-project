import {ControllerInterface} from './ControllerInterface';
import {RequestParam} from '../server';
import {ServerResponse} from 'http';
import {AbstractController} from './AbstractController';
import {OrderService} from '../services/order.service';
import {OrderInterface} from '../models/order.model';
import {OrderCommentController} from './orderComment.controller';
import {ChannelService} from '../services/channel.service';
import {ChannelInterface} from '../models/channel.model';

export class ChannelController extends AbstractController implements ControllerInterface {
    private orderCommentController: OrderCommentController;

    constructor() {
        super();
        this.orderCommentController = new OrderCommentController();
    }
    /**
     * @see ControllerInterface.getRoutes
     */
    getRoutes() {
        return {
            'channels': {
                //POST: this.createOrder,
                GET: this.listChannels,
                ':channelId': {
                    DELETE: this.deleteChannel,
                    // GET: this.getOrderDetails,
                    ...this.orderCommentController.getRoutes(),
                }
            },
        }
    }

    private listChannels(requestParams: RequestParam, res: ServerResponse): Promise<ChannelInterface[]> {
        const orderService = new OrderService();
        const channelService = new ChannelService();

        const orderId = this.checkParamUnicity(requestParams.pathParams[':orderId']);
        // TODO check mandatory inputs

        return channelService.findForOrder(orderId)
            .then(chan => chan)
            .catch(err => {
                this.logger.error('error when listing channeld' + err.message);
                this.errorService.mongooseToAPIerror(err);
                return [];
            });
    }

    private deleteChannel(requestParams: RequestParam, res: ServerResponse): Promise<boolean> {
        const channelService = new ChannelService();

        const channelId = this.checkParamUnicity(requestParams.pathParams[':channelId']);
        // TODO check mandatory inputs

        // optimistic approach : we choose to try to insert and then catch exception instead of trying to check manually if entity exists
        return channelService.delete(channelId)
            .then(chan => chan)
            .catch(err => {
                this.logger.error('error when deleting channel' + err.message);
                this.errorService.mongooseToAPIerror(err);
                return err;
            });
    }

}
