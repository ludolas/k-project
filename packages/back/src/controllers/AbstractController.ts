import {ServerResponse} from 'http';
import {HttpError} from '../errors/http.error';
import {ParameterError} from '../errors/parameter.error';
import {AppModules} from '../index';
import {ErrorService} from '../services/error.service';
import {RequestParam} from '../server';
import {Logger} from 'winston';

export class AbstractController {

    protected errorService: ErrorService;
    protected logger: Logger;

    constructor() {
        this.errorService = AppModules.errorService;
        this.logger = AppModules.logger;
    }

    /**
     * catches an error thrown by application and end server response if appropriate
     * @param err
     * @param res
     * @protected
     */
    protected catchHttpError(err: Error, res: ServerResponse) {
        if (err instanceof HttpError) {
            res.statusCode = err.httpCode;
            res.end(err.message);
        }
    }

    /**
     * checks if a get or post parameter is really unique, throws if multiple found, returns the only instance if ok
     *
     * @param param
     * @protected
     */
    protected checkParamUnicity<T>(param: T|T[]): T {
        if (Array.isArray(param)) {
            // global parameter error, could be extended with unicityParameter, FormatParameters ... error
            throw new ParameterError('Only one "name" parameter allowed');
        }
        return param;
    }

    /**
     * for debug purpose only
     * @param requestParams
     * @param res
     * @protected
     */
    protected logRoute(requestParams: RequestParam, res: ServerResponse) {
        this.logger.debug('route called ' + JSON.stringify(requestParams));
    }
}
