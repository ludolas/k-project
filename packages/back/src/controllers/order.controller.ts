import {ControllerInterface} from './ControllerInterface';
import {RequestParam} from '../server';
import {ServerResponse} from 'http';
import {AbstractController} from './AbstractController';
import {OrderService} from '../services/order.service';
import {OrderInterface} from '../models/order.model';
import {OrderCommentController} from './orderComment.controller';
import {ChannelController} from './channel.controller';

export class OrderController extends AbstractController implements ControllerInterface {
    private orderService: OrderService;
    private orderCommentController: OrderCommentController;
    private channelController: ChannelController;

    constructor() {
        super();

        this.orderService = new OrderService();
        this.orderCommentController = new OrderCommentController();
        this.channelController = new ChannelController();
    }
    /**
     * @see ControllerInterface.getRoutes
     */
    getRoutes() {
        return {
            'orders': {
                POST: this.createOrder,
                GET: this.listOrders,
                ':orderId': {
                    GET: this.getOrderDetails,
                    ...this.orderCommentController.getRoutes(),
                    ...this.channelController.getRoutes(),
                }
            },
        }
    }

    private createOrder(requestParams: RequestParam, res: ServerResponse): Promise<OrderInterface|false> {
        const orderService = new OrderService();

        // TODO check mandatory inputs
        const orderTitle = requestParams.postParams['title'];

        // optimistic approach : we choose to try to insert and then catch exception instead of trying to check manually if entity exists
        return orderService.createOrder(orderTitle)
            .then(order => order)
            .catch(err => {
                this.logger.error('error when creating order' + err.message);
                this.errorService.mongooseToAPIerror(err);
                return false;
        });
    }

    private listOrders(requestParams: RequestParam, res: ServerResponse): Promise<OrderInterface[]> {
        const orderService = new OrderService();

        // TODO check mandatory inputs

        return orderService.list()
            .then(order => order)
            .catch(err => {
                this.logger.error('error when listing orders' + err.message);
                this.errorService.mongooseToAPIerror(err);
                return [];
            });
    }

    private getOrderDetails(requestParams: RequestParam, res: ServerResponse): Promise<OrderInterface> {
        this.logRoute(requestParams, res);
        // FIXME should be checked with userId from bearer
        const orderId = requestParams.pathParams[':orderId'];
        // FIXME
        const orderService = new OrderService();

        // TODO check mandatory inputs

        return orderService.get(orderId)
            .then(order => order)
            .catch(err => {
                this.logger.error('error when getting order details' + err.message);
                this.errorService.mongooseToAPIerror(err);
                return err;
            });
    }
}
