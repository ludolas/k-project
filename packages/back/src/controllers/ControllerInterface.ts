export interface ControllerInterface {
    /**
     * This method should define hierarchically all available routes for the current controller
     * Should return an object with variable depth but each leaf route should have at least one of GET, POST, PUT, DELETE
     * property that contains a callback function that will be called with 3 params : RequestUrl, RequestParameters, Response
     *
     * Example : to define PUT /user/:userId/addresses/:addressId, the returned object would be
     *  {
     *      'user': {
     *          ':userId': {
     *              'addresses'; { // could also merge AddressController routes
     *                  ':addressId': {
     *                      PUT: methodToCall
     *                  }
     *              }
     *          }
     *      }
     *  }
     */
    getRoutes(): any;
}
