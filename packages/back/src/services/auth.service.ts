import {KPService} from './kp-service';
import {UserService} from './user.service';
import {UserInterface} from '../models/user.model';
import {HttpError} from '../errors/http.error';

export class AuthService extends KPService {
    private userService: UserService;

    constructor() {
        super();
        this.userService = new UserService();
    }

    public async login(name: string): Promise<UserInterface|void> {
        const user = await this.userService.findUserByName(name);
        if (user) {
            this.logger.info('user logged in ' + user);
            return user;
        }
        return;
    }

    public async register(name: string, isAdmin: boolean = false): Promise<UserInterface|void> {
        let user = await this.userService.findUserByName(name);

        if(user){
            throw new HttpError(409, 'User already exists');
        } else {
            user = await this.userService.createUser(name, isAdmin);
        }

        return user;
    }

}
