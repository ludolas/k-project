import {KPService} from './kp-service';
import {OrderComment, OrderCommentInterface} from '../models/orderComment.model';
import {ChannelService} from './channel.service';
import {socket} from '../index';
import {Socket} from '../socket';


export class OrderCommentService extends KPService {
    private channelService: ChannelService;
    private socketService: Socket;

    constructor() {
        super();
        this.channelService = new ChannelService();
        this.socketService = socket;
    }

    public async createOrderComment(userId: string, orderId: string, content: string, geoReferenceId?: number): Promise<OrderCommentInterface> {
        const channel = await this.channelService.getOrCreateChannel(userId, orderId, geoReferenceId);
        return OrderComment.create({from: userId, channel: channel.id, content})
            .then(comm => {
                this.socketService.emit('newOrderComment', {channelId: channel.id})
                return comm;
            });
    }

    public async createInChannel(userId: string, channelId: string, content: string, geoReferenceId?: number): Promise<OrderCommentInterface> {
        return OrderComment.create({from: userId, channel: channelId, content})
            .then(comm => {
                this.socketService.emit('newOrderComment', {channelId: channelId})
                return comm;
            });;
    }

    public async list(orderId: string, geoReferenceId?: number): Promise<OrderCommentInterface[]> {
        const channels = await this.channelService.findForOrder(orderId, geoReferenceId);
        return OrderComment.find({channel: channels.map(c => c.id)});
    }

    public async listForUser(userId: string, orderId: string, geoReferenceId?: number): Promise<OrderCommentInterface[]> {
        const channels = await this.channelService.findForUserAndOrder(userId, orderId, geoReferenceId);
        return OrderComment.find({channel: channels.map(c => c._id)});
    }

    public async listForChannel(channelId: string, geoReferenceId?: number): Promise<OrderCommentInterface[]> {
        return OrderComment.find({channel: channelId});
    }
}
