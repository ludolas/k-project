import mongoose from 'mongoose';
import winston, { Logger} from 'winston';

export class KPService {
    protected mongoose;
    protected logger: Logger;

    constructor() {
        this.mongoose = mongoose.connection;
        this.logger = winston.loggers.get('KP_LOGGER');
    }
}
