import { UserService } from './user.service';
import { AppModules } from '../index';
const mockingoose = require('mockingoose');
import {User} from '../models/user.model';

describe('Test user.service.ts', () => {
    let service: UserService;
    const userName = 'Mocky';

    beforeAll(() => {
        service = new UserService();
        const _doc = {
            _id: '507f191e810c19729de860ea',
            name: userName
        }

        mockingoose(User).toReturn(_doc, 'save');

    });

    afterAll(done => {
        AppModules.httpServer.close();
        AppModules.mongoose.connection.close();
        done();
    });

    beforeEach( () => {
        service = new UserService();
    });

    test('should say', () => {
        const name = "Jest";
        expect(service.sayMyName(name)).toEqual({"text": "Hello Jest"});
    })

    test('should create',   async () => {
        expect(await service.createUser(userName)).toHaveProperty('name', userName);
    })

    test('should return false as mongoose throws error wehn creating', async () => {
        const name = "Jest";

        mockingoose(User).reset();

        mockingoose(User).toReturn(new Error('err') , 'save');
        await(expect(service.createUser(name)).rejects.toThrow());
    })

    test('should return userDetails', async () => {
        const userId = "6086843aea57568f09eec75b";
        const fakeDoc = {"_id": userId, name: 'fakeMongooseUser'};

        mockingoose(User).toReturn(fakeDoc, 'findOne');
        service.getUserDetails(userId).then(doc => {
            expect(JSON.parse(JSON.stringify(doc))).toMatchObject(fakeDoc);
        });
    })

    test('should find user from name', async () => {
        const fakeDoc = [
            {"_id": "6086843aea57568f09eec75b", name: 'fakeMongooseUser'},
            {"_id": "6086843aea57568f09eec75c", name: 'fakeMongooseUser2'}
        ];

        mockingoose(User).toReturn(fakeDoc, 'find');
        service.findUsersByName('fakeMongooseUser').then(doc => {
            expect(JSON.parse(JSON.stringify(doc))).toMatchObject(fakeDoc);
        });
    })
});
