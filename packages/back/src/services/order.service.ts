import {KPService} from './kp-service';
import {Order, OrderInterface} from '../models/order.model';


export class OrderService extends KPService {

    public async createOrder(orderTitle: string): Promise<OrderInterface> {
        return Order.create({title: orderTitle});
    }

    public async list(): Promise<OrderInterface[]> {
        return Order.find({});
    }

    public async get(orderId: string): Promise<OrderInterface> {
        return Order.findOne({_id: orderId}).orFail();
    }
}
