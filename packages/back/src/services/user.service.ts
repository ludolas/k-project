import {KPService} from './kp-service';
import {User, UserInterface} from '../models/user.model';

export class UserService extends KPService {
    sayMyName(name: string): {text: string} {
        return {
            text: "Hello " + name
        };
    }

    public async createUser(name: string, isAdmin: boolean = false): Promise<UserInterface> {
        return User.create({name, isAdmin});
    }

    private async loadUserById(id: string): Promise<UserInterface> {
        return User.findById(id).orFail();
    }

    public async findUsersByName(name: string): Promise<UserInterface[]> {
        return User.find({name: { $regex: '.*' + name + '.*' } });
    }

    public async findUserByName(name: string): Promise<UserInterface> {
        return this.findUsersByName(name).then(res => res[0]);
    }

    public async getUserDetails(userId: string): Promise<any> {
        return this.loadUserById(userId);
    }

    public async update(userId: string, name: string, isAdmin: boolean = false): Promise<UserInterface> {
        return User.findByIdAndUpdate(userId, {name, isAdmin}, {new: true}).orFail()
    }

    public async delete(userId: string): Promise<boolean> {
        return User.findByIdAndDelete(userId)
            .then(() => true);
    }
}
