import {AppModules} from '../index';
import winston from 'winston';
import {HttpError} from '../errors/http.error';

export class ErrorService {
    private logger: winston.Logger;

    constructor() {
        this.logger = AppModules.logger;
    }
    public manageError(e: Error) {
        this.logger.log('new error', e);
    }

    public mongooseToAPIerror(e: any) {
        debugger;
        // FIXME user instanceof
        if (e.name === 'MongoError') {
            if(e.code && e.code === 11000)
            throw new HttpError(400, 'entry already exists');
        }
    }
}
