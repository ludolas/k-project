import {KPService} from './kp-service';
import {Channel, ChannelInterface} from '../models/channel.model';

export class ChannelService extends KPService {

    public async getOrCreateChannel(userId: string, orderId: string, geoReferenceId?: number): Promise<ChannelInterface> {
            return this.get(userId, orderId, geoReferenceId)
                .catch(() => this.create(userId, orderId, geoReferenceId));
    }

    public async findForOrder(orderId: string, geoReferenceId?: number): Promise<ChannelInterface[]> {
        return Channel.find({order: orderId});
    }

    public async findForUserAndOrder(userId: string, orderId: string, geoReferenceId?: number): Promise<ChannelInterface[]> {
        return Channel.find({user: userId, order: orderId, geoReferenceId});
    }


    public async get(userId: string, orderId: string, geoReferenceId: number = 0): Promise<ChannelInterface> {
        const channelName = `o:${orderId},u:${userId},g:${geoReferenceId}`;
        return Channel.findOne({name: channelName}).orFail();
    }

    public async create(userId: string, orderId: string, geoReferenceId: number = 0): Promise<ChannelInterface> {
        const channelName = `o:${orderId},u:${userId},g:${geoReferenceId}`;
        return Channel.create({name: channelName, user: userId, order: orderId, geoReferenceId});
    }

    public async delete(channelId: string): Promise<boolean> {
         return   Channel.findByIdAndDelete(channelId)
                .then(() => true);
    }

}
