// import { UserController } from './user.controller';
import { Server } from './server';
import { AppModules } from './index';
import {HttpError} from './errors/http.error';
import {UserController} from './controllers/user.controller';
const httpMocks = require('node-mocks-http');
var MockReq = require('mock-req');
var MockRes = require('mock-res');


describe('Test server.ts', () => {
    let server: Server;
    beforeAll(() => {
        server = AppModules.httpServer;
    });

    afterAll(done => {
        AppModules.httpServer.close();
        AppModules.mongoose.connection.close();
        done();
    });

    test('should greet', async (done) => {
        let res = new MockRes({});
        var req = new MockReq({
            method: 'GET',
            url: 'http://127.0.0.1:3001/api/v1/users/sample?name=greetMePlease',

            // arbitrary properties:
            search: 'thing'
        });
        const result = await server.handleRequest([], req, res);
        expect(result).toEqual({"text": "Hello greetMePlease"});
        done();

    });

    test('should throw error on no method', async (done) => {
        let res = new MockRes({});
        var req = new MockReq({
            url: 'http://127.0.0.1:3001/api/v1/users?name=greetMePlease',

            // arbitrary properties:
            search: 'thing'
        });
        // mock instaéntiation defaultly set GET methods
        req.method = undefined;
        await expect(server.handleRequest([], req, res)).rejects.toThrow(new HttpError(400, 'no method specified'));
        done();
    });

    test('should throw error on empty URL', async (done) => {
        let res = new MockRes({});
        var req = new MockReq({
            url: '',

            // arbitrary properties:
            search: 'thing'
        });
        // mock instaéntiation defaultly set GET methods
        req.method = undefined;
        await expect(server.handleRequest([], req, res)).rejects.toThrow(new HttpError(404));
        done();
    });


    test('should parse path params', async (done) => {
        // FIXME by lack of time, we use an existing controller but we should use a mock controller
        /*const ctrlSpy = spyOn(UserController as any, 'getUserDetails');
        let res = new MockRes({});
        var req = new MockReq({
            url: 'http://127.0.0.1:3001/api/v1/users/fafafafaafa',
            method: 'GET',
        });
        await server.handleRequest([], req, res);

        expect(ctrlSpy).toHaveBeenCalledWith(req, res, {pathParams : { ':userId':  'fafafafaafa' }});*/
        done();
    });
});
