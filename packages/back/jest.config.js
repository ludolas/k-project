module.exports = {
  testEnvironment: 'node',
  preset: "@shelf/jest-mongodb",
  transform: {
    "\\.ts$": "ts-jest",
  },
  collectCoverage: true,
  coverageDirectory: './cov',
  setupFilesAfterEnv: ['./jest.setup.js'],
};
