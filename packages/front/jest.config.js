module.exports = {
  testEnvironment: 'jsdom',
  transform: {
    "\\.ts$": "ts-jest",
    "^.+\\.vue$": "vue-jest",
    "^.+\\js$": "babel-jest",
    '.+\\.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$': 'jest-transform-stub',
  },
  collectCoverage: true,
  coverageDirectory: './cov',
  moduleFileExtensions: ['vue', 'js', 'json', 'jsx', 'ts', 'tsx', 'node'],
  moduleNameMapper: {
    '^.+.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$': 'jest-transform-stub',
  }
};
