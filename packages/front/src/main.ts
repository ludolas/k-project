import { createApp } from 'vue'
import App from './App.vue';
import router from "./router";
import SocketIO from 'socket.io-client'
import 'reflect-metadata'; // needed for class-transformer

export const socket = SocketIO('http://localhost:3001');

console.log(process.env);
export const config = {
    api: {
        host: process.env.API_HOST || 'localhost',
        port: process.env.API_PORT || '3001',
        basePath: process.env.API_BASE_PATH || '/api/v1/',
        entryPoint: `http://${process.env.API_HOST || 'localhost'}:${process.env.API_PORT || '3001'}${process.env.API_BASE_PATH || '/api/v1/'}`
    }
}


createApp(App)
    //.use(socket)
    .use(router)
    .mount('#app');
