import {reactive, toRefs} from 'vue';
import { useAuthProvider } from "../modules/providers/auth.provider";
const mockedUseAuthProvider = useAuthProvider as jest.Mock;

let mockUser = {name: 'jest', isAdmin: true, _id: '12'};
//////////////////////////////////////////////
// FIXME should mock from modules/providers/__mocks__/ but can't get it working
interface AuthState {
    authenticating: boolean;
    user?: User;
    error?: Error;
}

const AUTH_KEY = 'kp_user';
const storedUser = window.localStorage.getItem(AUTH_KEY);

const state = reactive<AuthState>({
    authenticating: false,
    user: storedUser && JSON.parse(storedUser) as User || undefined,
    error: undefined,
});

jest.mock('../modules/providers/auth.provider', () => {
    console.log('what happened');
    const useAuthProvider = () => {
        console.log('whos calling');
        state.user = mockUser;
        const logout = () => {
            console.log('logging out')
            state.user = undefined;
        }
        return {...toRefs(state), logout};
    };
    return { useAuthProvider };
});
///////////////////////////////////////////////////////////////

import { shallowMount } from '@vue/test-utils'
import KPHeader from './Header.vue';
import {User} from '../models/user.model';


describe('test component', () => {

    beforeAll(() => {
        useAuthProvider()
    });

    it('should greet', async (done) => {


        const wrapper = shallowMount(KPHeader);
        expect(wrapper.html()).not.toContain('login')

        done();
    })

    it('should proposelogin', async (done) => {
        const { logout } = useAuthProvider();

        logout();

        const wrapper = shallowMount(KPHeader);
        expect(wrapper.html()).toContain('login')

        done();
    });
});
