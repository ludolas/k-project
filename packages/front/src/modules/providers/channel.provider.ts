import { reactive, ref, toRefs } from "vue";
import {Channel} from '../../models/channel.model';
import axios, {AxiosResponse} from 'axios';
import {plainToClass} from 'class-transformer';
import {config} from '../../main';

interface ChannelState {
    channels?: Channel[];
}

const state = reactive<ChannelState>({
    channels: [],
});

export const useChannelProvider = () => {
    const loading = ref(false);

    const getChannels = (orderId: string, geoReferenceId?: string): Promise<Channel[]> => {
        return axios.get(`${config.api.entryPoint}orders/${orderId}/channels`, {params:{geoReferenceId}})
            .then((resp: AxiosResponse<Channel[]>) => state.channels = plainToClass(Channel, resp.data))
            .catch(err => {console.log(err); throw err;});
    }

    const deleteChannel = (channelId: string): Promise<boolean> => {
        return axios.delete(`${config.api.entryPoint}channels/${channelId}`)
            .then((resp: AxiosResponse<boolean>) => resp.data === true)
            .catch(err => {console.log(err); throw err});
    }

    return {
        getChannels,
        deleteChannel,
        ...toRefs(state), // orders
    }
}
