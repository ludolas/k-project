import { reactive, ref, toRefs } from "vue";
import {Order} from '@/models/order.model';
import axios, {AxiosResponse} from 'axios';
import {useAuthProvider} from '@/modules/providers/auth.provider';
import {config} from '@/main';

interface AuthState {
    orders?: Order[];
}

const AUTH_KEY = 'kp_user';

const state = reactive<AuthState>({
    orders: [],
});

export const useOrderProvider = () => {
    const loading = ref(false);

    const createOrder = (orderTitle: string) => {
        const { user } = useAuthProvider();
        return axios.post(`${config.api.entryPoint}orders`, {title: orderTitle}, {
            headers: {
                Authorization: 'UserId ' + user?.value?._id // FIXME: Bearer + generalise with auth provider
            }
        });
    };

    const refreshOrders = () => axios.get(`${config.api.entryPoint}orders`)
            .then((orders: AxiosResponse<Order[]>) => state.orders = orders.data)
            .catch(err => {console.log(err); return []});

    return {
        createOrder,
        refreshOrders,
        ...toRefs(state), // orders
    }
}
