import {useAuthProvider} from './auth.provider';
import {User} from '../../models/user.model';

const AUTH_KEY = 'kp_user';

describe("Auth", () => {
    const userToStore: User = {
        _id: 'jestUserId',
        name: 'jest',
        isAdmin: true,
    };

    beforeAll(() => {
        const knownUser = window.localStorage.removeItem(AUTH_KEY);
    })

    afterAll(() => {
        //const knownUser = window.localStorage.removeItem(AUTH_KEY);
    })

    it('login should change state user', () => {
        const { user , setUser } = useAuthProvider();
        // user in state
        expect(user && user.value).toBeUndefined();

        setUser(userToStore, false);
        const storedUser = window.localStorage.getItem(AUTH_KEY);
        expect(storedUser).toBeNull();
        expect(user && user.value).toEqual(userToStore);
    });

    it('should load stored user', () => {
        const { user } = useAuthProvider();
        // user in state
        expect(user && user.value).toEqual(userToStore);
    });

    it('logout should change state user', () => {
        const { user , setUser, logout } = useAuthProvider();
        setUser(userToStore, false);

        // user in state
        expect(user && user.value).toEqual(userToStore);

        logout();
        const storedUser = window.localStorage.getItem(AUTH_KEY);
        expect(storedUser).toBeNull();
        expect(user && user.value).toBeUndefined();
    });

    it('test login with remmeber me', () => {
        const { user , setUser } = useAuthProvider();
        const knownUser = window.localStorage.getItem(AUTH_KEY);
        // user in state
        expect(user && user.value).toBeUndefined();
        expect(knownUser).toBeNull();

        setUser(userToStore, true);
        const storedUser = window.localStorage.getItem(AUTH_KEY);
        expect(storedUser).toEqual(JSON.stringify(userToStore));
    });

    it('logout should remove storage user', () => {
        const { user , setUser, logout } = useAuthProvider();
        setUser(userToStore, true);

        // user in state
        // expect(user && user.value).toEqual(userToStore);

        let storedUser = window.localStorage.getItem(AUTH_KEY);
        expect(storedUser).toEqual(JSON.stringify(userToStore));

        logout();
        storedUser = window.localStorage.getItem(AUTH_KEY);
        expect(storedUser).toBeNull();
        expect(user && user.value).toBeUndefined();
    });


});
