import { reactive, ref, toRefs } from "vue";
import {User} from '@/models/user.model';
import axios, {AxiosResponse} from 'axios';
import {config} from '@/main';

interface AuthState {
    user?: User;
}

const state = reactive<AuthState>({
    user: undefined,
});

export const useUserProvider = () => {
    const loading = ref(false);

    const getUserDetails = (userId: string) => axios.get(`${config.api.entryPoint}users/${userId}`)
        .then((resp: AxiosResponse<User>) => state.user = resp.data)
        .catch(err => {console.log(err); return []});

    const updateUser = (user: User) => axios.put(`${config.api.entryPoint}users/${user._id}`, {...user})
        .then((resp: AxiosResponse<User>) => state.user = resp.data)
        .catch(err => {console.log(err); return []});

    const deleteUser = (userId: string) => axios.delete(`${config.api.entryPoint}users/${userId}`)
        .then((resp: AxiosResponse<boolean>) => { state.user = undefined; return resp;})
        .catch(err => {console.log(err); return err});

    return {
        getUserDetails,
        updateUser,
        deleteUser,
        ...toRefs(state), // user
    }
}
