import mockAxios from 'jest-mock-axios';
import {useChannelProvider} from './channel.provider';

describe('test Channel Provider', () => {

    const channelsStub = [{"_id":"608d7cee8fb3e572e4909e5a","name":"o:608d7ae1d2e4597260cdce06,u:608d7ae6d2e4597260cdce07,g:0","user":"608d7ae6d2e4597260cdce07","order":"608d7ae1d2e4597260cdce06","geoReferenceId":0,"__v":0},{"_id":"608d7cf28fb3e572e4909e5c","name":"o:608d7ae1d2e4597260cdce06,u:608d7ae6d2e4597260cdce07,g:9","user":"608d7ae6d2e4597260cdce07","order":"608d7ae1d2e4597260cdce06","geoReferenceId":9,"__v":0},{"_id":"608d7d178fb3e572e4909e61","name":"o:608d7ae1d2e4597260cdce06,u:608d7d0e8fb3e572e4909e5e,g:1","user":"608d7d0e8fb3e572e4909e5e","order":"608d7ae1d2e4597260cdce06","geoReferenceId":1,"__v":0},{"_id":"608d9ee2dbc1db7cbc6f3de7","name":"o:608d7ae1d2e4597260cdce06,u:608d9ed2dbc1db7cbc6f3de6,g:5","user":"608d9ed2dbc1db7cbc6f3de6","order":"608d7ae1d2e4597260cdce06","geoReferenceId":5,"__v":0},{"_id":"608d9f30dbc1db7cbc6f3deb","name":"o:608d7ae1d2e4597260cdce06,u:608d9ed2dbc1db7cbc6f3de6,g:0","user":"608d9ed2dbc1db7cbc6f3de6","order":"608d7ae1d2e4597260cdce06","geoReferenceId":0,"__v":0}]


    beforeAll(() => {

    })

    afterEach(() => {
        // cleaning up the mess left behind the previous test
        mockAxios.reset();
    });

    it('should retrieve fake channels', async (done) => {
        const { channels, getChannels } = useChannelProvider();
        //axios.get.mockResolvedValue('data');

        const catchFn = jest.fn(),
            thenFn = jest.fn();
        getChannels('12')
            .then(thenFn)
            .catch(catchFn);

        expect(channels && channels.value).toEqual([]);

        mockAxios.mockResponse({data: channelsStub});

        getChannels('12');
        expect(channels && channels.value).toEqual(channelsStub);
        expect(thenFn).toHaveBeenCalled();
        done();
    });

    it('state should have kept channels',  () => {
        const { channels, getChannels } = useChannelProvider();
        expect(channels && channels.value).toEqual(channelsStub);
    });

    it('on error should keep channels',  async (done) => {
        const { channels, getChannels } = useChannelProvider();
        expect(channels && channels.value).toEqual(channelsStub);

        let catchFn = jest.fn(),
            thenFn = jest.fn();
        getChannels('12')
            .then(thenFn)
            .catch(catchFn);

        mockAxios.mockError(new Error('ouch'));

        getChannels('12');

        expect(catchFn).toHaveBeenCalled();
        done();
    });


    it('should delete channels', async (done) => {
        const { channels, deleteChannel } = useChannelProvider();
        //axios.get.mockResolvedValue('data');

        const catchFn = jest.fn(),
            thenFn = jest.fn();
        deleteChannel('azerty')
            .then(thenFn)
            .catch(catchFn);

        mockAxios.mockResponse({data: true});

        deleteChannel('azerty');
        expect(channels && channels.value).toEqual(channelsStub);
        expect(thenFn).toHaveBeenCalled();
        done();
    });

    it('state should have kept channels',  () => {
        const { channels, getChannels } = useChannelProvider();
        expect(channels && channels.value).toEqual(channelsStub);
    });

    it('on error should keep channels',  async (done) => {
        const { channels, deleteChannel } = useChannelProvider();
        expect(channels && channels.value).toEqual(channelsStub);



        const catchFn = jest.fn(),
            thenFn = jest.fn();
        deleteChannel('azerty')
            .then(thenFn)
            .catch(catchFn);

        mockAxios.mockError(new Error('ouch'));
        deleteChannel('azerty');

        expect(catchFn).toHaveBeenCalled();
        done();
    });
});
