import { reactive, ref, toRefs } from "vue";
import {User} from '@/models/user.model';
import axios, {AxiosResponse} from 'axios';
import {config} from '@/main';

interface AuthState {
    users?: User[];
}

const state = reactive<AuthState>({
    users: [],
});

export const useUsersProvider = () => {
    const loading = ref(false);

    const refreshUsers = (search: string = '') => axios.get(`${config.api.entryPoint}users`, {params: { name: search }})
            .then((resp: AxiosResponse<User[]>) => state.users = resp.data)
            .catch(err => {console.log(err); return []});

    return {
        refreshUsers,
        ...toRefs(state), // orders
    }
}
