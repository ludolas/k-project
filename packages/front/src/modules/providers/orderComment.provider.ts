import { reactive, ref, toRefs } from "vue";
import {OrderComment} from '@/models/orderComment.model';
import axios, {AxiosResponse} from 'axios';
import {useAuthProvider} from '@/modules/providers/auth.provider';
import {config} from '@/main';

interface OrderCommentState {
    comments?: OrderComment[];
}

const state = reactive<OrderCommentState>({
    comments: [],
});

export const useOrderCommentProvider = () => {
    const loading = ref(false);
    const { user } = useAuthProvider();

    const createOrderCommentInChannel = (channelId: string, comment: string, zoneId: number = 0) =>
        axios.post(`${config.api.entryPoint}channels/${channelId}/comments`,
            {geoReferenceId: zoneId, content: comment},
            {
                headers: {
                    Authorization: 'UserId ' + user?.value?._id //the token is a variable which holds the token
                }
            }
    );

    const createOrderCommentForOrder = (orderId: string, comment: string, zoneId: number = 0) =>
        axios.post(`${config.api.entryPoint}orders/${orderId}/comments`,
        {geoReferenceId: zoneId, content: comment},
        {
            headers: {
                Authorization: 'UserId ' + user?.value?._id //the token is a variable which holds the token
            }
        }
    );

    const refreshOrderComments = (orderId: string, geoReferenceId?: number, channelId?: string) => {
        const { user } = useAuthProvider();
        const userId = user?.value && user.value._id;
        if (!channelId) {
            axios.get(`${config.api.entryPoint}orders/${orderId}/comments`, {
                params: {geoReferenceId},
                headers: {
                    Authorization: 'UserId ' + user?.value?._id //the token is a variable which holds the token
                }
            })
                .then((orderComments: AxiosResponse<OrderComment[]>) => state.comments = orderComments.data)
                .catch(err => {
                    console.log(err);
                    return []
                });
        } else {
            // admin
            axios.get(`${config.api.entryPoint}channels/${channelId}/comments`, {
                params: {geoReferenceId},
            })
                .then((orderComments: AxiosResponse<OrderComment[]>) => state.comments = orderComments.data)
                .catch(err => {
                    console.log(err);
                    return []
                });
        }
    }

    return {
        createOrderCommentInChannel,
        createOrderCommentForOrder,
        refreshOrderComments,
        ...toRefs(state), // orders
    }
}
