import { reactive, toRefs } from "vue";
import {User} from '../../models/user.model';
import axios from 'axios';
import {config} from '../../main';

interface AuthState {
    authenticating: boolean;
    user?: User;
    error?: Error;
}

// FIXME put in env
const AUTH_KEY = 'kp_user';
const storedUser = window.localStorage.getItem(AUTH_KEY);

const state = reactive<AuthState>({
    authenticating: false,
    user: storedUser && JSON.parse(storedUser) as User || undefined,
    error: undefined,
});

export const useAuthProvider = () => {

    const login = (username:string) =>
        axios.post(`${config.api.entryPoint}auth/login`, {name: username})

    const register = (username:string, isAdmin: boolean = false) =>
        axios.post(`${config.api.entryPoint}auth/register`,
            { name: username, isAdmin
            });

    const setUser = (payload: User, remember: boolean) => {
        if ( remember ) {
            // Save
            window.localStorage.setItem(AUTH_KEY, JSON.stringify(payload))
        }

        state.user = payload
        state.error = undefined
    }

    const logout = (): Promise<void> => {
        window.localStorage.removeItem(AUTH_KEY)
        return Promise.resolve(state.user = undefined)
    }

    return {
        register,
        login,
        setUser,
        logout,
        ...toRefs(state), // authenticating, user, error
    }
}
