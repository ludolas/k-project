export interface ChannelInterface {
    _id: string;
    geoReferenceId: string;
    userId: string; // userId
    orderId: string; // userId
}

export class Channel implements ChannelInterface {
    _id!: string;
    geoReferenceId!: string;
    orderId!: string;
    userId!: string;

}
