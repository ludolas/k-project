export interface Order {
    _id: string;
    geoReferenceId: string;
    owner: string; // userId
}
