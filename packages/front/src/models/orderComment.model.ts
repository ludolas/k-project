export interface OrderComment {
    _id: string;
    orderId: string;
    geoReferenceId: string;
    content: string;
    _createdAt: string;
    from: string; // userId
}
