import Login from '@/components/Login.vue';
import {createRouter, createWebHashHistory } from 'vue-router'
import OrderComments from '@/components/OrderComments.vue';
import Home from '@/components/Home.vue';
import Register from '@/components/Register.vue';
import CreateOrder from '@/components/CreateOrder.vue';
import {useAuthProvider} from '@/modules/providers/auth.provider';
import Users from '@/components/Users.vue';
import UserDetails from '@/components/UserDetails.vue';

const router = createRouter({
    history: createWebHashHistory(),
    routes: [
        { path: '/', name: 'login', component: Login },
        { path: '/register', name: 'register', component: Register, props: (route) => ({ desiredUserName: route.query.desiredUserName }) },
        { path: '/hello', name: 'home', component: Home, meta: { requiresAuth: true } },
        { path: '/orders/create', name: 'newOrder', component: CreateOrder, meta: { requiresAuth: true }},
        { path: '/orders/:orderId', name: 'comments', component: OrderComments, meta: { requiresAuth: true }},
        { path: '/users', name: 'users', component: Users, meta: { requiresAuth: true, requiresAdmin: true  }},
        { path: '/users/:userId', name: 'userDetails', component: UserDetails, meta: { requiresAuth: true, requiresAdmin: true }},
    ]
})

router.beforeEach((to, from, next) => {
    const { user } = useAuthProvider()

    // Not logged into a guarded route?
    if (to.meta.requiresAuth && !user?.value ){
        next({ name: 'login' });
        return;
    }

    // Logged in for an auth route
    else if ( (to.name == 'login' || to.name == 'register') && user!.value ) {
        next({ name: 'home' });
        return;
    }

    const isAdmin = user && user?.value && user.value.isAdmin;
    console.log('isadmin');
    if (to.meta.requiresAdmin && !isAdmin) {
        next({ name: 'home' })
        return;
    }

    // Carry On...
    else next()
})

export default router;
