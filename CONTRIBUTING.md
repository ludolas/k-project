[[_TOC_]]

## Devlopment Environement ##
### Pre-Requesites ###
#### Globally installed packages ####

* [Node with npm](https://nodejs.org/en/download/) (tested with node v10.23.0, npm 6.14.11)
* lerna (tested with 3.22.1) :
```npm i -g lerna```
* [mongoDb](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-os-x/#installing-mongodb-4.4-edition-edition) (tested with CE 4.4) 

### Installation ###
* from project root directory, execute ```lerna bootstrap```

### launch for development ###
#### backend ####
* Mongo should be launched
* ```packages/back$ npm run start:dev```
* use start:inspect to plug a debugger
#### frontend ####
* ```packages/front$ npm run serve```

### test ###
note : a lerna npm target build each package is available as alpha, not tested
#### backend ####
* ```packages/back$ npm test```
#### frontend ####
* ```packages/front$ npm test```

### Build ###
note : a lerna npm target build each package is available as alpha, not tested 
#### backend ####
* ```packages/back$ npm run build```
* use start:inspect to plug a debugger
#### frontend ####
* ```packages/front$ npm run serve```

### launch built with docker ###
docker should be installed
* checkout desired branch
* run ```docker compose up .``` from project root
* add -d to run detached, --force-recreate --build if you changed source code
* you should be able to hit http://localhost:8080 

# Process #
We use git flow, master reflects production, develop must be stable and accepts only stable merge request,
 for developping a new feature, first take it on JIRA, create the associated issue in gitlab and merge request starting from develop
submit merge request when development is finished  
there sometimes can be a 'release' branch to fix some issues on RC


# TODO #
## to aim production ##
By lack of time, several things still needs to be done : 
* be stateless server side, websocket connections are stored in memory
* there might be log access issues as its path is hard written

### front ###
* aria tags
* i18n
* env variables
* review components file organization
* use sub router when a left menu is available
* check user entries
* store bearer only
* add e2e tests (cucumber)

### back ###
* check user entries
* store bearer only
* check user session 
* advanced rights management for API routes

### build ###
* deploy packages on registry

### container ###
* use volume for mongo db

… and a lot more !! 
